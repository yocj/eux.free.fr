#!/usr/bin/python3

import requests
import sys
# sys.argv=['', 'prenoms.txt', 'free.fr', 'eux.html']
if len(sys.argv)<4:
    print(f"usage: {sys.argv[0]} fichierListePrenom domain fichierSauvegardeHtml")
    sys.exit(1)

def analyze(liste, domaine):
    total=len(liste)
    listePresents = []
    for ind,prenom in enumerate(liste):
        print(f'analyze {ind}/{total}')
        if requests.get(f'http://{prenom}.{domaine}').status_code == 200:
            listePresents.append(f'<a href="http://{prenom}.{domaine} target=_blank>{prenom}</a><br>\n')
    return listePresents

def sauve(liste, fichier):
    with open(fichier, 'w') as f:
        f.write('<html>\n<body>\n')
        for prenom in liste:
            f.write(prenom)
        f.write('</body>\n</html>')

listePrenomBrute = open(sys.argv[1], 'r').readlines()
listePrenom = [p.strip() for p in listePrenomBrute]
res = analyze(listePrenom, sys.argv[2])
sauve(res, sys.argv[3])
